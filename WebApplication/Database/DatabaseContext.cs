﻿using Microsoft.EntityFrameworkCore;
using WebApplication.Models;

namespace WebApplication.Database
{
	public sealed class DatabaseContext : DbContext
	{
		public DbSet<Entity> Entities { get; set; }

		public DatabaseContext()
		{
			Database.EnsureCreated();
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite("Filename=data.db");
		}

	}
}