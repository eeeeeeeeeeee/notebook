﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="/Content/modal-style.css" />
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <title>Notebook</title>
</head>
<body>

    <form id="form1" runat="server">
        <div class="container" id="app">
            
            <!-- Add entity -->
            <modal v-if="addModal" @close="addModal = false">
                <h3 slot="header">Add field</h3>
                <div slot="body">
                    <input v-model="name" class="form-control" placeholder="Фамилия Имя Отчество" type="text" value="">
                    <input v-model="mail" class="form-control" placeholder="E-Mail" type="text" value="">
                    <input v-model="phone" class="form-control" placeholder="Phone" type="text" value="">
                    <div class="input-group-text">
                        <label>Male <input type="radio" v-model="sex" value="true"></label><br/>
                        <label>Female <input type="radio" v-model="sex" value="false"></label>
                    </div>
                        
                    <input class="btn btn-primary" type="button" value="Accept" @click="acceptAdd();"/>
                    
                    <button class="btn btn-dark" @click="$emit('close')">
                        Close
                    </button>
                </div>
                <div slot="footer">
                    
                </div>
            </modal>
            <!-- / Add entity -->
            
            <!-- Edit entity -->
            <modal v-if="editModal" @close="editModal = false">
                <h3 slot="header">Edit field</h3>
                <div slot="body">
                    <input v-model="name" class="form-control" placeholder="Фамилия Имя Отчество" type="text" value="">
                    <input v-model="mail" class="form-control" placeholder="E-Mail" type="text" value="">
                    <input v-model="phone" class="form-control" placeholder="Phone" type="text" value="">
                    <div class="input-group-text">
                        <label>Male <input type="radio" v-model="sex" value="true"></label><br/>
                        <label>Female <input type="radio" v-model="sex" value="false"></label>
                    </div>
                        
                    <input class="btn btn-primary" type="button" value="Accept" @click="acceptEdit()"/>
                    
                    <button class="btn btn-dark" @click="$emit('close')">
                        Close
                    </button>
                </div>
                <div slot="footer">
                    
                </div>
            </modal>
            <!-- / Edit entity -->

            <table class="table text-center table-hover">
                <h1 class="text-center">Notebook</h1>
                <div class="container table-warning">
                    {{ error }}
                </div>
                <thead class="thead-dark">
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Mail</th>
                    <th>Phone</th>
                    <th>Sex</th>
                    <th>Change:</th>
                    <th v-on:click="add()"><svg width="1.4em" height="1.4em" viewBox="0 0 16 16" class="bi bi-file-earmark-plus" fill="white" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9 1H4a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h5v-1H4a1 1 0 0 1-1-1V3a1 1 0 0 1 1-1h5v2.5A1.5 1.5 0 0 0 10.5 6H13v2h1V6L9 1z"/>
                        <path fill-rule="evenodd" d="M13.5 10a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1H13v-1.5a.5.5 0 0 1 .5-.5z"/>
                        <path fill-rule="evenodd" d="M13 12.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0v-2z"/>
                    </svg></th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="field in info">
                    <td>{{ field.Id }}</td>
                    <td>{{ field.Name }}</td>
                    <td>{{ field.Mail }}</td>
                    <td>{{ field.Phone }}</td>
                    <td>{{ field.Male?'Male':'Female' }}</td>
                    <td v-on:click="edit(field)">
                        <svg width="1.4em" height="1.4em" viewBox="0 0 16 16" class="bi bi-file-earmark-diff" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M4 1h5v1H4a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V6h1v7a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2z"/>
                            <path d="M9 4.5V1l5 5h-3.5A1.5 1.5 0 0 1 9 4.5z"/>
                            <path fill-rule="evenodd" d="M5.5 11.5A.5.5 0 0 1 6 11h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5zM8 5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0v-4A.5.5 0 0 1 8 5z"/>
                            <path fill-rule="evenodd" d="M5.5 7.5A.5.5 0 0 1 6 7h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                    </td>
                    <td v-on:click="remove(info, field)">
                        <svg width="1.4em" height="1.4em" viewBox="0 0 16 16" class="bi bi-file-earmark-minus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M9 1H4a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h5v-1H4a1 1 0 0 1-1-1V3a1 1 0 0 1 1-1h5v2.5A1.5 1.5 0 0 0 10.5 6H13v2h1V6L9 1z"/>
                            <path fill-rule="evenodd" d="M11 11.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
    

    <!-- Window template -->
    <script type="text/x-template" id="modal-template">
        <transition name="modal">
            <div class="modal-mask">
                <div class="modal-wrapper">
                    <div class="modal-container">

                        <div class="modal-header">
                            <slot name="header">
                                default header
                            </slot>
                            </div>

                            <div class="modal-body">
                            <slot name="body">
                                 default body
                            </slot>
                            </div>

                            <div class="modal-footer">
                            <slot name="footer">
                                default footer
                                <button class="btn btn-dark" @click="$emit('close')">
                                OK
                                </button>
                            </slot>
                        </div>
                    </div>
                </div>
            </div>
        </transition>
     </script>
    

    <!-- Vue application -->
    <script>
        Vue.component("modal", {
            template: "#modal-template"
        });

        var app = new Vue({
            el: '#app',
            data() {
                return {
                    id: 0,
                    name: "",
                    phone: "",
                    mail: "",
                    sex: "false",

                    addModal: false,
                    editModal: false,
                    info: null,
                    error: null
                };
            },
            methods: {
                edit: function (field) {
                    this.id = field.Id;
                    this.name = field.Name;
                    this.phone = field.Phone;
                    this.mail = field.Mail;
                    this.sex = field.Male ? "true" : "false";

                    this.editModal = true;
                },
                acceptEdit: function (event) {
                    axios
                        .get('/api/entity/update?id=' + this.id + "&name=" + this.name + "&mail=" + this.mail + "&phone=" + this.phone + "&sex=" + this.sex)
                        .then(response => (
                            this.errorMessage(response.data)
                        ));
                },
                errorMessage: function (resp) {
                    if (!resp.Status) {
                        this.error = resp.Message;
                    } else {
                        this.error = "Successfully";
                    }
                },
                remove: function (info, field) {
                    axios
                        .get('/api/entity/remove?id='+field.Id)
                        .then(response => (
                            this.acceptRemove(info, field, response.data)
                        ));
                },
                acceptRemove: function (info, field, response) {
                    if (response.Status) {
                        var index = info.indexOf(field);
                        info.splice(index, 1);
                    } else {
                        this.error = response.Message;
                    }
                },
                add: function () {
                    this.name = "";
                    this.phone = "";
                    this.mail = "";
                    this.sex = "true";

                    this.addModal = true;
                },
                acceptAdd: function () {
                    axios
                        .get('/api/entity/add?name=' + this.name + "&mail=" + this.mail + "&phone=" + this.phone + "&sex=" + this.sex)
                        .then(response => (
                            this.errorMessage(response.data)
                        ));
                }
            },
            mounted() {
                axios
                    .get('/api/entity/load')
                    .then(response => (this.info = response.data))
                    .catch(error => console.log(error));
            }
        });
    </script>
</body>
</html>
