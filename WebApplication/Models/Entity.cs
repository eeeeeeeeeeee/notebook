﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.RegularExpressions;

namespace WebApplication.Models
{
	public class Entity
	{
		public int Id { get; set; }

		[Column(TypeName = "varchar(100)")]
		public string Name {
			get => _name;
			set {
				var r = new Regex("^[A-zА-я]+ [A-zА-я]+ [A-zА-я]+$");
				_name = value;
				if (value == null || !r.IsMatch(value)) AddTrouble(Trouble.Name);
			}
		}

		[Column(TypeName = "varchar(20)")]
		public string Phone {
			get => _phone;
			set {
				var r = new Regex("^[+]{0,1}\\d{11}$");
				_phone = value;
				if (value == null || !r.IsMatch(value)) AddTrouble(Trouble.Phone);
			}
		}

		[Column(TypeName = "varchar(100)")]
		public string Mail {
			get => _mail;
			set {
				var r = new Regex("^\\w+@\\w+[.]\\w+$");
				_mail = value;
				if (value == null || !r.IsMatch(value)) AddTrouble(Trouble.Mail);
			}
		}

		public bool Male { get; set; }

		private string _name;
		private string _phone;
		private string _mail;

		private enum Trouble
		{
			Mail, Phone, Name
		}

		private List<Trouble> _troubles;

		private void AddTrouble(Trouble t) {
			if (_troubles is null) _troubles = new List<Trouble>();
			_troubles.Add(t);
		}

		public bool IsCorrect() {
			return _troubles is null || _troubles.Count<1;
		}

		public string GetTroubleMessage()
		{
			if (IsCorrect()) return "All right";

			var sb = new StringBuilder();
			if (_troubles.Contains(Trouble.Mail)) sb.AppendLine("Incorrect mail");
			if (_troubles.Contains(Trouble.Name)) sb.AppendLine("Incorrect name");
			if (_troubles.Contains(Trouble.Phone)) sb.AppendLine("Incorrect phone");
			return sb.ToString();
		}
	}
}