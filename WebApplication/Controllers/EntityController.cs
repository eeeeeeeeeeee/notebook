﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WebApplication.Controllers.CEntity;
using WebApplication.Database;
using WebApplication.Models;

namespace WebApplication.Controllers
{
	public class EntityController : ApiController
	{
		[HttpGet]
		[Route("api/entity/add")]
		public EditingAnswer Add(string name, string phone, string mail, bool sex)
		{
			var entity = new Entity
			{
				Name = name,
				Phone = phone,
				Mail = mail,
				Male = sex
			};

			if (!entity.IsCorrect()) return new EditingAnswer
			{
				Status = false,
				Message = entity.GetTroubleMessage()
			};

			using (var context = new DatabaseContext())
			{
				context.Entities.Add(entity);
				context.SaveChanges();
				return new EditingAnswer
				{
					Status = true,
					Field = entity
				};
			}
		}

		[HttpGet]
		[Route("api/entity/update")]
		public EditingAnswer Update(int id, string name, string phone, string mail, bool sex) {
			var entity = new Entity {
				Id = id,
				Name = name,
				Phone = phone,
				Mail = mail,
				Male = sex
			};

			if (!entity.IsCorrect()) return new EditingAnswer {
				Status = false,
				Message = entity.GetTroubleMessage()
			};

			using (var context = new DatabaseContext()) {
				context.Entities.Update(entity);
				context.SaveChanges();
				return new EditingAnswer {
					Status = true,
					Field = entity
				};
			}
		}

		[HttpGet]
		[Route("api/entity/remove")]
		public EditingAnswer Remove(int id) {
			using (var context = new DatabaseContext()) {
				try {
					var entity = context.Entities.Single(i => i.Id == id);
					context.Remove(entity);
					context.SaveChanges();
					return new EditingAnswer {
						Status = true
					};
				} catch (Exception e) {
					return new EditingAnswer {
						Status = false,
						Message = e.Message
					};
				}
			}
		}

		[HttpGet]
		[Route("api/entity/load")]
		public List<Entity> Load()
		{
			using (var context = new DatabaseContext())
			{
				return new List<Entity>(context.Entities);
			}
		}


	}
}