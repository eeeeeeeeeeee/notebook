﻿using WebApplication.Models;

namespace WebApplication.Controllers.CEntity
{
	public sealed class EditingAnswer
	{
		public bool Status { get; set; }
		public string Message { get; set; }
		public Entity Field { get; set; }
	}
}